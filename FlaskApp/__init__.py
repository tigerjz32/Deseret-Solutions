from flask import Flask, render_template, request, redirect, url_for
from flask.ext.assets import Environment, Bundle
import requests


app = Flask(__name__)
assets = Environment(app)

js = Bundle('jquery.js', 'base.js', 'widgets.js',
            filters='jsmin', output='gen/packed.js')
assets.register('js_all', js)

@app.route('/')
def index():
	return render_template('template.html')

if __name__ == '__main__':
  app.run()


